/**
 *  Pop out message on scroll detect element
 * 
 *  Adapted by Barry Conlon. @Baz_Conlon http://www.barryconlon.com
 *
 *  Adapted from http://www.benknowscode.com/2013/07/detect-dom-element-scrolled-with-jquery.html
 *  Created by Ben Olson
 */
(function($){
    var _watch = [];

    function monitor( element, options ) {
        var item = { element: element, options: options, invp: false };
        _watch.push(item);
        return item;
    }

    var $window = $(window),
        _buffer = null;

    $window.on('scroll', function ( e ) {

        if ( !_buffer ) {

            _buffer = setTimeout(function () {

                checkInView( e );

                _buffer = null;

           }, 300);
        }

    });

    function viewportLimits($el) {
        var docViewTop = $window.scrollTop(),
             docViewBottom = docViewTop + $window.height(),
             elemTop = $el.offset().top,
             elemBottom = elemTop + $el.height();

        return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)
               && (elemBottom <= docViewBottom) &&  (elemTop >= docViewTop) );
    }

    function checkInView( e ) {

        $.each(_watch, function () {

            if ( viewportLimits( this.element ) ) {
                if ( !this.invp ) {
                    this.invp = true;
                    if ( this.options.scrolledin ) this.options.scrolledin.call( this.element, e );
                    this.element.trigger( 'scrolledin', e );
                }
            } 
            else if ( this.invp ) {
                this.invp = false;
                if ( this.options.scrolledout ) this.options.scrolledout.call( this.element, e );
                this.element.trigger( 'scrolledout', e );
            }
            
        });
    }

    var pluginName = 'scrolledIntoView',
        settings = {
        scrolledin: null,
        scrolledout: null
    }

    $.fn[pluginName] = function( options ) {

    var options = $.extend({}, settings, options);

    this.each( function () {

        var $el = $(this),
            instance = $.data( this, pluginName );

        if ( instance ) {
            instance.options = options;
        } else {
            $.data( this, pluginName, monitor( $el, options ) );
        }
    });

    return this;
    }

    $( document ).ready(function() {

        $('#popout-trigger')
            .scrolledIntoView()
            .on('scrolledin', function () { 
                $( '#recommended-popout' ).css( "opacity", "1" );
            })
            .on('scrolledout', function () {
                $( '#recommended-popout' ).css( "opacity", "0" );
            });

        $('#popout-close').on("touch, click", function(){
        	$( '#recommended-popout' ).css( 'opacity', '0' )
                .addClass( "closed");
        });
     });
})(jQuery);