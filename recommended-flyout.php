<?php
/*
Plugin Name: Recommended Flyout Banner
Plugin URI: http://www.barryconlon.com
Description: A plugin which creates a recommended flyout post at the bottom of content on an article page 
Version: 1.0.0
Author: Barry Conlon
Author URI: http://www.barryconlon.com
License: GPL2
*/

add_image_size( 'flyout-thumb', 86, 64, true);

function enqueue_flyout_scripts()
{
	if ( is_single() )
	{	
		wp_enqueue_script( 'flyout_js',
			plugins_url( '/js/flyout.js',  __FILE__ ),
			array( 'jquery' ) );

		wp_enqueue_style( 'flyout_css',
			plugins_url( '/css/flyout.css',  __FILE__ ),
			array(), '20120206' );
	}
}

if ( false === is_admin() )
{
	add_action( 'wp_enqueue_scripts' , 'enqueue_flyout_scripts' );

	add_action ('the_content', 'display_flyout');
}

function display_flyout($content)
{
	if ( is_single() )
	{
		//$recommended_post_title = get_post_meta($post->ID,'recommended_post_title');

		$previous_post = get_previous_post();
		$next_post = get_next_post();
		$previous_post_thumb = get_the_post_thumbnail($previous_post->ID, 'flyout-thumb' );
		$next_post_thumb = get_the_post_thumbnail($next_post->ID, 'flyout-thumb' );

		$url = get_permalink( $previous_post->ID );
		$title = $previous_post->post_title;

		$return_flyout = 
			'<div id="popout-trigger"></div>
			<div id="recommended-popout">
				<h3>Recommended for you</h3>
				<div id="popout-close">X</div>
				<p><a href="' . $url . '">' . $previous_post_thumb . ' ' . $title . '</a></p>
			</div>';
		return $content . $return_flyout;
	}
}