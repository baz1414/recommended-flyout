# README #

Recommended Flyout is a Wordpress plugin for displaying a flyout banner at the end of the_content() on blog posts.


### How do I get set up? ###

* Simply add this folder to your wordpress plugins folder and you are ready to go
* Additionally, if you want to change any styling then you should do so in the flyout.css file


### Who do I talk to? ###

* Repo owner or admin